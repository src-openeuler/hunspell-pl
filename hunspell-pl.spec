Name:        hunspell-pl
Summary:     Hunspell dictionaries for Polish
Version:     0.20240701
Release:     1
URL:         https://sjp.pl/slownik/ort/
Source:      https://sjp.pl/sl/ort/sjp-myspell-pl-20240701.zip
License:     LGPL-2.0-or-later or GPL-1.0-or-later or MPL-1.1 or Apache-2.0 or CC-BY-SA-4.0
BuildArch:   noarch

Requires:    hunspell
Supplements: (hunspell and langpacks-pl)

%description
Hunspell dictionaries for Polish.

%prep
%autosetup -c hunspell-pl -p1

%build
unzip pl_PL.zip

%install
install -d $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell


%files
%doc README_pl_PL.txt
%{_datadir}/myspell/*

%changelog
* Wed Jul 10 2024 yaoxin <yao_xin001@hoperun.com> - 0.20240701-1
- Update to 0.20240701

* Mon Nov 21 2022 caodongxia <caodongxia@h-partners.com> - 0.20200327-5
- Modify invalid source0

* Wed Jul 06 2022 wangkai <wangkai385@h-partners.com> - 0.20200327-4
- License compliance rectification

* Tue Apr 14 2020 lizhenhua <lizhenhua21@huawei.com> - 0.20200327-3
- Package init
